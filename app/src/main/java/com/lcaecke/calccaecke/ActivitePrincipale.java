package com.lcaecke.calccaecke;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Locale;

public class ActivitePrincipale extends AppCompatActivity {


    TextView result, error_msg;
    EditText input1;
    EditText input2;
    RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setLogo(R.mipmap.ic_launcher_foreground);
            actionBar.setDisplayUseLogoEnabled(true);
        }

        this.input1 = findViewById(R.id.value1);
        this.input2 = findViewById(R.id.value2);
        this.radioGroup = findViewById(R.id.radioGroup);
        this.result = findViewById(R.id.result);
        this.error_msg = findViewById(R.id.error_msg);
    }


    public void onRazClick(View v) {

        // Clear inputs
        this.input1.getText().clear();
        this.input2.getText().clear();

        // Clear radiogroup
        this.radioGroup = findViewById(R.id.radioGroup);
        this.radioGroup.clearCheck();

        // Clear printed msgs
        this.result.setText("");
        this.error_msg.setText("");
    }

    public void onCalcCLick(View v) {

        try {
            float val1 = Float.parseFloat(input1.getText().toString());
            float val2 = Float.parseFloat(input2.getText().toString());
            float result = this.makeOperation(val1, val2);

            this.result.setText( String.valueOf(result) );
            this.error_msg.setText("");
        } catch (NumberFormatException e) {
            this.error_msg.setText(R.string.incorrect_number);
        } catch (Exception e) {
            this.error_msg.setText(e.getMessage());
        }
    }

    private float makeOperation(float val1, float val2) throws Exception {
        switch (radioGroup.getCheckedRadioButtonId()) {

            case R.id.radio_plus:
                return val1 + val2;

            case R.id.radio_minus:
                return val1 - val2;

            case R.id.radio_mult:
                return val1 * val2;

            case R.id.radio_div:
                if (val2 == 0) {
                    throw new Exception(getString(R.string.div_zero));
                }
                return val1 / val2;

            default:
                this.result.setText("");
                throw new Exception(getString(R.string.no_selected_operation));
        }
    }


    public void exitApp(View v) {
        finish();
        System.exit(0);
    }
}